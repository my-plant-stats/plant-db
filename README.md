# Moved to CouchDB instance instead

Justfollow those septs to setup the db backend:

## 2. Prerequisite for the Raspberry Pi. #

	sudo apt-get update
	sudo apt-get dist-upgrade

---

## 3. Download, build and install software. #

	wget http://packages.erlang-solutions.com/debian/erlang_solutions.asc
	sudo apt-key add erlang_solutions.asc
	sudo apt-get update
	
	sudo apt-get --no-install-recommends -y install build-essential \
	pkg-config erlang libicu-dev \
	libmozjs185-dev libcurl4-openssl-dev
	
	sudo useradd -d /home/couchdb couchdb
	sudo mkdir /home/couchdb
	sudo chown couchdb:couchdb /home/couchdb
	
	cd

	!! get the latest and correct version here: http://couchdb.apache.org/#download !!
	
	wget http://mirror.ibcp.fr/pub/apache/couchdb/source/3.1.1/apache-couchdb-3.1.1.tar.gz   
	
	tar zxvf apache-couchdb-3.1.1.tar.gz
	cd apache-couchdb-3.1.1/
	
	./configure
	make release
	
	cd ./rel/couchdb/
	sudo cp -Rp * /home/couchdb
	sudo chown -R couchdb:couchdb /home/couchdb
	
	cd
	rm -R apache-couchdb-3.1.1/
	rm apache-couchdb-3.1.1.tar.gz
	rm erlang_solutions.asc

---

## 4. First run and check. #

For remote access:

	sudo nano /home/couchdb/etc/local.ini

=> change line

	#bind_address = 127.0.0.1
	#port = 5984
to:

	bind_address = 0.0.0.0
	port = 4000

To the end of your etc/local.ini file, after the [admins] line, add the text admin = password, so it looks like this:
	
	[admins]
	admin = password

(! not prefixed by # !)

Now, you are ready to run CouchDB as couchdb user with:
	
	sudo -i -u couchdb /home/couchdb/bin/couchdb

Verify your installation via the 

---

## 5. Login on FauxtonUI and create partitioned db named "plantstats". #

[http://127.0.0.1:4000/_utils/#/_config](http://127.0.0.1:4000/_utils/#/_config)

Move to the properties of the new DB and change the permissions to public by deleting all roles and users.
> Database members can access the database. If no members are defined, the database is public.
---

## 6. Script for running CouchDB on boot. #

	mkdir /var/log/couchdb/
	sudo chown couchdb:couchdb /var/log/couchdb
	
Then, be sure that CouchDB is still running and in your Browser go into the Configuration page: [http://127.0.0.1:4000/_utils/#/_config](http://127.0.0.1:4000/_utils/#/_config). If you have defined an administrator you should be logged in as the administrator.

Click on the +Add Option and fill the form like here:

values are:

- `log` for Section,
- `file` for Name and 
- `/var/log/couchdb/couch.log` for Value.

Click on Create.

Now, create the service by editing a new file:

	sudo nano /lib/systemd/system/couchdb.service

and paste the following to the content of the editor:

	[Unit]
	Description=CouchDB Service
	After=network.target
	  
	[Service]
	Type=idle
	User=couchdb
	Restart=always
	ExecStart=/home/couchdb/bin/couchdb
	  
	[Install]
	WantedBy=default.target
	
Then, fix file permissions with:

	sudo chmod 644 /lib/systemd/system/couchdb.service

and instruct systemd to start the service during the boot sequence:

	sudo systemctl daemon-reload
	sudo systemctl enable couchdb.service

When you reboot the Pi the couchdb service should run:

	sudo reboot

and check service status using:

	sudo systemctl status couchdb.service

The above instructions are modified for the project. Further details can be found here: 
Credits: joal @[CouchDB Setup on Raspberry with Autostart](https://www.hackster.io/imed-ch/installing-couchdb-on-raspbian-stretch-ccb2a7)

//// OLD

## Plant-DB 

root directory for a plantdb-server

run from terminal directly from project root via `pouchdb-server --port 4000`

edit `config.json` to change url

use Fauxton in browser `http://localhost:4000/_utils/#/_all_dbs`

## Entry in pouchDB

doc style in pouchDB

```
{
        _id: new Date().toISOString(),
        moisture: moisture,
        temperature: temperature,
        lighting: lighting,
        humidity: humidity
    }
```

### Seting up systemctl service 

to run the server on start edit inside `pouchDBService.service` the path to `your/plant-db` where `restart.sh` is located

init the server by copying it to eg

`sudo cp /home/pi/projects/plant-db/pouchDBService.service /lib/systemd/system/`

run it via

`sudo systemctl start pouchDBService.service`

check it before

`sudo systemctl status pouchDBService.service`

setting it up autostart 

`sudo systemctl enable pouchDBService.service`
